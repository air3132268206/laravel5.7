# noinspection SqlNoDataSourceInspectionForFile

-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: 10.7.123.135:3306
-- Generation Time: 2018-10-13 12:30:30
-- 服务器版本： 5.7.20
-- PHP Version: 7.1.16-1+b1


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aircms2`
--
-- --------------------------------------------------------

--
-- 表的结构 `ac_migrations`
--

DROP TABLE IF EXISTS `ac_migrations`;
CREATE TABLE `ac_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `ac_migrations`
--

INSERT INTO `ac_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(6, '2016_06_01_000004_create_oauth_clients_table', 2),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- 表的结构 `ac_oauth_access_tokens`
--

DROP TABLE IF EXISTS `ac_oauth_access_tokens`;
CREATE TABLE `ac_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `ac_oauth_access_tokens`
--

INSERT INTO `ac_oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0746d2421cbbcd0d6d71ab59489230b3102ddce992fbc36c2445a41c0de165887a6f4e09914e2ce4', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:46:54', '2018-10-12 08:46:54', '2018-10-13 16:46:54'),
('1b9e0a44a9ff1ca93b98f43689727c6170b666753b4558cc2bf939ffa75db6162e8570937b060bde', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 07:48:13', '2018-10-12 07:48:13', '2018-10-13 15:48:13'),
('2ace78dfa10fa1d4ad8add70b7e247eca8e198af99fd9b778e28653e8f52e746287545f64fd003bb', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 07:48:00', '2018-10-12 07:48:00', '2018-10-13 15:48:00'),
('6ca8594f44ce2fd912a6ca7cfb5e3c143c51f0a652084d7ed37868b0bd9808221d99bd287152dc1b', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:52:24', '2018-10-12 08:52:24', '2018-10-13 16:52:24'),
('707a38c74f485845747f70a0ff9d8ba1c960ae60d87a243706c55d7c9edbd4fc460d22827225e3f3', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 07:48:11', '2018-10-12 07:48:11', '2018-10-13 15:48:11'),
('7b52dd7e83e7214c4900a5b80573ffa59e2a8578328da36c19a98c8570bac645e8df00dbf749c3dc', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 07:48:13', '2018-10-12 07:48:13', '2018-10-13 15:48:13'),
('8daa4211b46b4ce5e28f312ff104d06ef3038008242693df073e4a3bec3931ad4ec36762d5e3423c', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 07:48:15', '2018-10-12 07:48:15', '2018-10-13 15:48:15'),
('9e3ec72708296ed7db03751be8e19836df1bd148c8c84b2d208fec848a0d46894795b8db3a66be1e', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:51:35', '2018-10-12 08:51:35', '2018-10-13 16:51:35'),
('9e4e4729bdc3392352f2ebf598eeef59e288aa196b99ec65c1f60b0daf970445db6b306095622f44', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:57:18', '2018-10-12 08:57:18', '2018-10-13 16:57:18'),
('baf569f34ed3466d15d69a47bc2d9f44069801e436d5bcb8df79c1949854d592a9327d1fe0e76ca4', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:46:56', '2018-10-12 08:46:56', '2018-10-13 16:46:56'),
('db38009a46d70e32948104cabfbc46579e717430fe0fcf2bd172c2a6ed490ad6f688e4d6a8cd3245', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:46:55', '2018-10-12 08:46:55', '2018-10-13 16:46:55'),
('e28da7982692f60217e102680351c7286aa732496697a9a791cf4f89173aaaaf82c2feb626f11c12', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 07:47:09', '2018-10-12 07:47:09', '2018-10-13 15:47:09'),
('e76ed27a41bc021cf6e71fd9ff15b72fa929c4218d8b9c17e869e885d71ccbacb889249753677b0a', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:32:28', '2018-10-12 08:32:28', '2018-10-13 16:32:28'),
('eed9b9d204ff664af7e400748ea205cf19d2b022c99ad98041b75b0180b4f99c2375d57f8cd38ecd', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 07:48:16', '2018-10-12 07:48:16', '2018-10-13 15:48:16'),
('f7936afdeb76cdb720cf4464521e722f011b5bd0a51ec8bf1f78cc89e53b49b2df1d5ab8698b862e', 1, 4, NULL, '[\"*\"]', 0, '2018-10-12 08:56:40', '2018-10-12 08:56:40', '2018-10-13 16:56:40');

-- --------------------------------------------------------

--
-- 表的结构 `ac_oauth_auth_codes`
--

DROP TABLE IF EXISTS `ac_oauth_auth_codes`;
CREATE TABLE `ac_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `ac_oauth_clients`
--

DROP TABLE IF EXISTS `ac_oauth_clients`;
CREATE TABLE `ac_oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `ac_oauth_clients`
--

INSERT INTO `ac_oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'OrM8NJOlYnGOtj5I4PSyCoQC1JJ5dwrpSEA9TRcm', 'http://localhost', 1, 0, 0, '2018-10-12 06:18:29', '2018-10-12 06:18:29'),
(2, NULL, 'Laravel Password Grant Client', '9hKIVw4IIbpy8NYApZ3Z3JRLnbhxNvwk9YH7T262', 'http://localhost', 0, 1, 0, '2018-10-12 06:18:29', '2018-10-12 06:18:29'),
(3, NULL, 'air', 'iphyXTgOzB0JzHwhnQYVODciwQONxXe5goWRy0Lz', 'http://localhost', 0, 1, 0, '2018-10-12 06:44:30', '2018-10-12 06:44:30'),
(4, NULL, 'api', 'hdh79bHRJfg7SOOlXmMdBd1kVOLm2r3fxMnZRJp8', 'http://localhost', 0, 1, 0, '2018-10-12 07:09:38', '2018-10-12 07:09:38'),
(5, NULL, 'air', 'gE8D61tp4lVDxnuG6xPCUTuqr4bRFm5NZtUMDLdw', 'http://localhost', 1, 0, 0, '2018-10-12 07:15:20', '2018-10-12 07:15:20');

-- --------------------------------------------------------

--
-- 表的结构 `ac_oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `ac_oauth_personal_access_clients`;
CREATE TABLE `ac_oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `ac_oauth_personal_access_clients`
--

INSERT INTO `ac_oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-10-12 06:18:29', '2018-10-12 06:18:29'),
(2, 5, '2018-10-12 07:15:20', '2018-10-12 07:15:20');

-- --------------------------------------------------------

--
-- 表的结构 `ac_oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `ac_oauth_refresh_tokens`;
CREATE TABLE `ac_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `ac_oauth_refresh_tokens`
--

INSERT INTO `ac_oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('021af8b97175b661e412cfcff5e1b2644994178c9d521780ac9ef10b772444feefbeef89b4146a3b', 'eed9b9d204ff664af7e400748ea205cf19d2b022c99ad98041b75b0180b4f99c2375d57f8cd38ecd', 0, '2018-11-11 15:48:16'),
('282c9a1a1c08ba3618ecba1b1b3c861b01324b3f45c0d36167b72c2e2aa15c5c8f82925210773dc9', 'db38009a46d70e32948104cabfbc46579e717430fe0fcf2bd172c2a6ed490ad6f688e4d6a8cd3245', 0, '2018-11-11 16:46:55'),
('4704eba01bcd97d6329e9e7b679f937b47687cddb770b52bf477e00e1fc85e55063178595bdd4ea7', '9e3ec72708296ed7db03751be8e19836df1bd148c8c84b2d208fec848a0d46894795b8db3a66be1e', 0, '2018-11-11 16:51:35'),
('4ad7a68d8d7eae179cf483b00330b5bd93237f3c4fdf88b3a17c70d19e8de82abd8f1db1d6887919', '0746d2421cbbcd0d6d71ab59489230b3102ddce992fbc36c2445a41c0de165887a6f4e09914e2ce4', 0, '2018-11-11 16:46:54'),
('82cb6cf5f803c79068abd81a0d2716b4daca367d7d0ac9913a8a8bc6cce45a58599cd95a772faefd', 'baf569f34ed3466d15d69a47bc2d9f44069801e436d5bcb8df79c1949854d592a9327d1fe0e76ca4', 0, '2018-11-11 16:46:56'),
('9333cb1a988c69a7aef1810e2b9201f8a4278d86835818a18ab916405951353489dd1e3a49ccb96b', '6ca8594f44ce2fd912a6ca7cfb5e3c143c51f0a652084d7ed37868b0bd9808221d99bd287152dc1b', 0, '2018-11-11 16:52:24'),
('94fbfc9b60a7a9eb2ceaf177dbebf9f6be4f6c9b06cc582abd777fc67d3064918e5fa374066785c1', 'f7936afdeb76cdb720cf4464521e722f011b5bd0a51ec8bf1f78cc89e53b49b2df1d5ab8698b862e', 0, '2018-11-11 16:56:40'),
('9fda8c2dc5e8383d5230f59fb0303fc5f0e5929467de4408ce7b7e9d8585c36823a3d99f6b1f1d2f', '8daa4211b46b4ce5e28f312ff104d06ef3038008242693df073e4a3bec3931ad4ec36762d5e3423c', 0, '2018-11-11 15:48:15'),
('b639fe77e81f308f26dad37232db526effa44ad20b591cec685a5f0264fa9ae6f475b155fd0d76f7', '1b9e0a44a9ff1ca93b98f43689727c6170b666753b4558cc2bf939ffa75db6162e8570937b060bde', 0, '2018-11-11 15:48:13'),
('c2f2e50944e8991c322dbf289da4b029187e825a7f017b40e258d1252aeee84150962da3508e3eff', 'e76ed27a41bc021cf6e71fd9ff15b72fa929c4218d8b9c17e869e885d71ccbacb889249753677b0a', 0, '2018-11-11 16:32:28'),
('c80b24584bae9018c02cb568aeba7cae1c7abf42ccd134ebd0a621fba4cca69f993ceb24f09a31e4', '2ace78dfa10fa1d4ad8add70b7e247eca8e198af99fd9b778e28653e8f52e746287545f64fd003bb', 0, '2018-11-11 15:48:00'),
('cc63827a2e538358802a4512d35ce33af33905f9f3fddc150eea20fc4c10e23c0bba450636154831', '7b52dd7e83e7214c4900a5b80573ffa59e2a8578328da36c19a98c8570bac645e8df00dbf749c3dc', 0, '2018-11-11 15:48:13'),
('ccb566511a4737641c56719bef49189a6ae2dd8c6e7b9b644af206fb2616598365853c2f2877f27f', 'e28da7982692f60217e102680351c7286aa732496697a9a791cf4f89173aaaaf82c2feb626f11c12', 0, '2018-11-11 15:47:10'),
('cf1bf0e6c13b35c063763c331371a749370e7c2507bb734aea55bfac274ca85d552b75484bf89e9c', '9e4e4729bdc3392352f2ebf598eeef59e288aa196b99ec65c1f60b0daf970445db6b306095622f44', 0, '2018-11-11 16:57:18'),
('e8271ca6c567018c1e5e8d7748d375a05f12f806d6179058dfa0bc1eab6fa012711206f2080dfb2f', '707a38c74f485845747f70a0ff9d8ba1c960ae60d87a243706c55d7c9edbd4fc460d22827225e3f3', 0, '2018-11-11 15:48:11');

-- --------------------------------------------------------

--
-- 表的结构 `ac_password_resets`
--

DROP TABLE IF EXISTS `ac_password_resets`;
CREATE TABLE `ac_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `ac_users`
--

DROP TABLE IF EXISTS `ac_users`;
CREATE TABLE `ac_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `ac_users`
--

INSERT INTO `ac_users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'air', 'air@qq.com', NULL, '$2y$10$fSEg0UTispUNTMNMhpV0OOmWo5eFkf1Iu5M9FHMy2bnwgGvl3Sh8y', NULL, '2018-10-12 07:30:56', '2018-10-12 07:30:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ac_migrations`
--
ALTER TABLE `ac_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ac_oauth_access_tokens`
--
ALTER TABLE `ac_oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `ac_oauth_auth_codes`
--
ALTER TABLE `ac_oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ac_oauth_clients`
--
ALTER TABLE `ac_oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `ac_oauth_personal_access_clients`
--
ALTER TABLE `ac_oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `ac_oauth_refresh_tokens`
--
ALTER TABLE `ac_oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `ac_password_resets`
--
ALTER TABLE `ac_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `ac_users`
--
ALTER TABLE `ac_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `ac_migrations`
--
ALTER TABLE `ac_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- 使用表AUTO_INCREMENT `ac_oauth_clients`
--
ALTER TABLE `ac_oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `ac_oauth_personal_access_clients`
--
ALTER TABLE `ac_oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `ac_users`
--
ALTER TABLE `ac_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
