<?php

namespace App\utils;

/**
 * Created by PhpStorm.
 * User: air
 * Date: 18-10-13
 * Time: 下午4:29
 */

class CommonUtils
{
    /**
     * 多维数组查找
     * @param $value
     * @param $array
     * @return bool
     */
    static public  function deep_in_array($value, $array) {
        foreach($array as $item) {
            if(!is_array($item)) {
                if ($item == $value) {
                    return true;
                } else {
                    continue;
                }
            }
            if(in_array($value, $item)) {
                return true;
            } else if(self::deep_in_array($value, $item)) {
                return true;
            }
        }
        return false;
    }


    static public function deep_array($array){
        if(!is_array($array)){
            return 0;
        }
        $i=1;$j=1;
        foreach($array as $vo){
            if(is_array($vo)){
                $i=1+self::deep_array($vo);
                if($j < $i){
                    $j = $i;
                }
            }
        }
        return $j;
    }

    /**
     * 判断字符串是否是整数
     * @param $str
     * @return bool
     */
    static public function is_int($str){
        return ((int)$str==$str&&is_numeric($str)==true)?true:false;
    }


    /**
     * 获取一定长度的随机数
     * @param $length
     * @param string $type 默认返回数字+大小写字母 ；0-》纯数字 ，1-》纯小写字母，2-》纯大写字母，3-》数字+大小写字母+特殊字符
     * @createTime 2016年7月14日18:02:23
     */
    static public function s_rand($length,$type=-1){
        switch($type){
            case 0:    // 纯数字随机数
                $str='0123456789';
                break;
            case 1;   // 纯小写英文字母
                $str='abcdefghijklmnopqrstuvwxyz';
                break;
            case 2:   // 纯大写英文字母
                $str='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 3:   // 包涵特殊字符的随机数
                $str='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*~-+=()';
                break;
            default:  // 默认数字+小写字母+大写字母
                $str='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
        }
        $resStr='';
        for($i=0;$i<$length;$i++){
            $resStr.=$str[rand(0,strlen($str)-1)];
        }
        return $resStr;
    }

    /**
     * 表单提交时验证，防止表单重复提交的简单验证，
     * 限定表单在10秒内只能提交一次
     * @param int $time
     * @return bool
     */
    static public function check_form_session($time=10){
        if(isset($_SESSION['form_verify_time'])&&$_SESSION['form_verify_time']){
            if((time()-$_SESSION['form_verify_time'])<$time){
                return FALSE;
            }else{
                $_SESSION['form_verify_time']=time();
            }
        }else{
            $_SESSION['form_verify_time']=time();
        }
        return TRUE;
    }


    /**
     * @param $filename    要解压的zip文件
     * @param $path        解压后存放的路径
     */
    static function unzip($filename, $path) {
        //先判断待解压的文件是否存在
        if (!file_exists($filename)) {
            die("文件 $filename 不存在！");
        }
        $starttime = explode(' ', microtime()); //解压开始的时间

        //将文件名和路径转成windows系统默认的gb2312编码，否则将会读取不到
        $filename = iconv("utf-8", "gb2312", $filename);
        $path = iconv("utf-8", "gb2312", $path);
        //打开压缩包
        $resource = zip_open($filename);
        $i = 1;
        //遍历读取压缩包里面的一个个文件
        while ($dir_resource = zip_read($resource)) {
            //如果能打开则继续
            if (zip_entry_open($resource, $dir_resource)) {
                //获取当前项目的名称,即压缩包里面当前对应的文件名
                $file_name = $path . zip_entry_name($dir_resource);
                //以最后一个“/”分割,再用字符串截取出路径部分
                $file_path = substr($file_name, 0, strrpos($file_name, "/"));
                //如果路径不存在，则创建一个目录，true表示可以创建多级目录
                if (!is_dir($file_path)) {
                    mkdir($file_path, 0777, TRUE);
                }
                //如果不是目录，则写入文件
                if (!is_dir($file_name)) {
                    //读取这个文件
                    $file_size = zip_entry_filesize($dir_resource);
                    //最大读取20M，如果文件过大，跳过解压，继续下一个
                    if ($file_size < (1024 * 1024 * 20)) {
                        $file_content = zip_entry_read($dir_resource, $file_size);
                        file_put_contents($file_name, $file_content);
                    }
                    else {
                        echo "<p> " . $i++ . " 此文件已被跳过，原因：文件过大， -> " . iconv("gb2312", "utf-8", $file_name) . " </p>";
                    }
                }
                //关闭当前
                zip_entry_close($dir_resource);
            }
        }
    }


    /**
     * 加密（可逆）
     * @param $str
     * @param $key
     * @param string $method
     * @return string
     */
    static public function encrypt($str,$key,$method='DES-CBC',$iv=''){
        if($iv==''){
            $iv=$key;
        }
        $data=openssl_encrypt($str, $method,$key,1,$iv);
        $encrypt=base64_encode($data);
        return $encrypt;
    }


    /**
     * 解密
     * @param $encrypt
     * @param $key
     * @param string $method
     * @return int|string
     */
    static public function decrypt($encrypt,$key,$method='DES-CBC',$iv='') {
        $encrypt = base64_decode($encrypt);
        if($iv==''){
            $iv=$key;
        }
        $decrypt = openssl_decrypt($encrypt, $method, $key, 1,$iv);
        if($decrypt){
            return $decrypt;
        }else{
            return 0;
        }
    }

    /**
     * @param $url 请求网址
     * @param bool $params 请求参数
     * @param int $ispost 请求方式
     * @param int $https https协议
     * @param string $authorization api认证头部信息
     * @return bool|mixed
     */
    static public  function curl($url, $params = false, $ispost = 0, $authorization=NULL,$https = 0)
    {
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if($authorization){
            $headr = array();
            $headr[] = 'Authorization: '.$authorization;
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headr);
        }


        if ($https) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 对认证证书来源的检查
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // 从证书中检查SSL加密算法是否存在
        }
        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params) {
                if (is_array($params)) {
                    $params = http_build_query($params);
                }
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }

        $response = curl_exec($ch);
        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);

        return ($httpCode>=200 && $httpCode<300) ? $response : false;
    }


    /**
     * 判断是否是手机号
     * @param $num
     * @return bool
     */
    static  public function is_tel($num){
        if(preg_match("/^1[3456789]{1}\d{9}$/",$num)){
            return true;
        }else{
            return false;
        }
    }


    /**
     * 获取ip地址
     * @return string
     */
    static public function get_ip(){
        $ip=false;
        if(!empty($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
            for ($i = 0; $i < count($ips); $i++) {
                if (!preg_match ("^(10│172.16│192.168).", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }

}


