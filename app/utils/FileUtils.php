<?php
namespace App\utils;
use \Illuminate\Support\Facades\File;

/**
 * Created by PhpStorm.
 * User: air
 * Date: 18-10-14
 * Time: 下午2:48
 */
final class FileUtils{

    /**
     * 删除文件
     * @param $path
     * @return mixed
     */

    static public function deleteFile($path){
        if(!File::isFile($path)){                                               // 文件不存在
            return FALSE;
        }
        return File::delete($path);
    }

    /**
     * 获取文件夹及其子文件夹下面的所有文件
     * @param $path
     */
    static public function allFiles($path){
        if(!is_dir($path)){                                                    // 文件夹不存在
            return FALSE;
        }
        return File::allFiles($path);
    }


    /**
     * 移动文件夹
     * @param string $directory
     * @param string $destination
     * @param int|NULL $options
     * @return bool
     */
    static public function copyDirectory(string $directory, string $destination, int $options = null){
        if(!is_dir($directory)){
            return FALSE;
        }
        return File::copyDirectory( $directory,  $destination,$options);
    }

    /**
     * 删除文件夹
     * @param $directory
     * @return bool
     */
    static public function deleteDirectory($directory){
        if(!is_dir($directory)){
            return FALSE;
        }
        return File::deleteDirectory($directory);
    }

    /**
     * 复制文件
     * @param $directory
     * @return bool
     */
    static public function copy(string $from, string $to){
        if(!File::isFile($from)){
            return FALSE;
        }
        return File::copy($from,$to);
    }


    /**
     * 追加文件内容
     * @param $directory
     * @return bool
     */
    static public function append(string $path, string $data){
//        if(!File::isFile($path)){
//            return FALSE;
//        }

        return File::append($path,$data);
    }

    /**
     * 追加文件内容(头部)
     * @param $directory
     * @return bool
     */
    static public function prepend(string $path, string $data){
//        if(!File::isFile($path)){
//            return FALSE;
//        }

        return File::prepend($path,$data);
    }


    /**
     * 写入内容到文件中
     * @param $directory
     * @return bool
     */
    static public function put(string $path, string $contents, string $visibility = null){
//        if(!File::isFile($path)){
//            return FALSE;
//        }
        return File::put($path,$contents,$visibility);
    }


    /**
     * 创建文件夹
     * @param $directory
     * @return bool
     */
    static public function makeDirectory($directory){
        $dirArr = explode('/', str_replace('\\', '/', $directory));
        $temArr = '';
        foreach ($dirArr as $key => $vo){
            if($key){
                $temArr .= '/' . $vo;
            }
            else{
                $temArr = $vo;
            }
            if(is_dir(storage_path($temArr))){
                continue;
            }
            else{
                if(!File::makeDirectory(storage_path($temArr))){
                    return FALSE;
                }
            }
        }

        return TRUE;
    }



}